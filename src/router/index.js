import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/MagicCardList.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
    },
    {
      path: '/test',
      name: 'testCard',
      component: () => import('../components/MagicCardDetails.vue')
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
