# axios-magic

## Magic Card Open API 
All Cards [GET] [ALLCARDS](https://api.magicthegathering.io/v1/cards).

One Card [GET]  [ONE CARD](https://api.magicthegathering.io/v1/cards/5f8287b1-5bb6-5f4c-ad17-316a40d5bb0c).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
